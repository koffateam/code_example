# Пример кода
Проект содержит пример кода с использованием фреймворка Laravel

## Установка и запуск
### Системные зависимости:
- docker
- docker-compose
- make

### Шаги установки:
1.Клонируйте репозиторий
```bash
git clone https://Koffa@bitbucket.org/koffateam/code_example.git
cd code_example
```

2.Копируйте переменные окружения
```bash
cp .env.development .env
```

3.Запустите docker-контейнеры проекта
```bash
make up
```

4.Установите composer зависимости
```bash
make composer install
```

5.Запустите тестовую команду
```bash
make command test:test
```


## Окружение
Окружение проекта построено на базе docker-контейнеров и docker-compose. 
Для удобства разворачивания и управления окружением используется Makefile.
`docker-compose-development.yml` файл находится в корне проекта.
Dockerfile-ы находятся в дирректории `code_example/docker`.
Список контейнеров: backend, postgres, redis, cron, horizon.

## Сервисы и участки кода, на которые стоит обратить внимание

### Типизированные коллекции Laravel
`code_example/app/Extend/Illuminate/Support/Collection/TypedCollection.php`

Базовые коллекции Laravel расширены, в них добавлена возможность указывать тип элементов, 
которые может содержать данная коллекция. Это расширение служит для реализации чего-то похожего на дженерики.
Пример использования: `code_example/app/Entities/PSR7/RequestsCollection.php`


### Сервис HttpSender
`code_example/app/Services/HttpSender`

Интерфейс `HttpSender` обязывает реализовать два метода: `sendRequest` и `sendRequests`. 
Эти методы спроектированы в соответствии со стандартом PSR-7. 
Метод `sendRequests` позволяет указывать кол-во потоков, которое должно быть использовано для отправки запросов.

На базе этого интерфейса возможны реализации с использованием:

* file_get_contents
* curl
* Guzzle
* Selenium

Представлена реализация интерфейса `HttpSender` - `GuzzleHttpSender`. 
В ней добавлено PSR-3 совместимое логирование и PSR-16 совместимое кэширование запросов.
Кроме того, реализован паттерн Circuit breaker.


### Сервис для запуска событий Laravel
`code_example/app/Services/Events`

Кроме немедленного выбрасывания события (метод `dispatch`) добавлено отложенное выбрасывание событий.
Это нужно для участков кода, где в рамках транзакции выбрасываются события, слушатели которых рассчитывают, что транзакция уже завершена при использовании уровня изоляции Read Committed.

Условный пример кода:
```php
<?php

use Illuminate\Support\Facades\DB;
use App\Helpers\Events;
use App\Events\SomeEvent;
use App\Models\SomeModel;

DB::beginTransaction();

try {
    foreach (range(1, 1000) as $n) {
        $someModel = new SomeModel;
        $someModel->save();
    
        /* слушатели события могут начать работу до завершения транзакции 
        и не смогут обратиться к только что сохраненному объекту SomeModel,
        потому что транзакция еще не завершена и используется уровень изоляции транзакций Read Committed */
        event(new SomeEvent($someModel));
    
        /* отложенное событие, фактическое его выбрасывание произойдет ниже по коду */
        Events::deferred(new SomeEvent($someModel));
    }
} catch (Throwable $e) {
    DB::rollBack();
    throw $e;
}

DB::commit();

/* выбрасывание всех отложенных событий */
Events::dispatchDeferred();
```

### Добавлены middleware для консольных команд
`code_example/app/Console/Commands/Command.php`

Все команды по умолчанию запускаются в рамках транзакции. 
Если в коде транзакция будет запущена повторно и БД поддерживает несколько уровней транзакции (например SAVEPOINT в Postgres), то будет создан новый уровень транзакции.
В конце выполнения каждой команды происходит завершение транзакций и выбрасывание всех отложенных событий.
