<?php

namespace App\Helpers;

use App\Constants\Env as EnvLib;

class Env
{
    public static function getEnv(): string
    {
        return app()->environment();
    }

    public static function isDevelopment(): bool
    {
        return static::getEnv() === EnvLib::DEVELOPMENT;
    }

    public static function isProduction(): bool
    {
        return static::getEnv() === EnvLib::PRODUCTION;
    }
}
