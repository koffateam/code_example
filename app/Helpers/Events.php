<?php

namespace App\Helpers;

use App\Events\Event;
use App\Services\Events\EventsService;

class Events
{
    public static function dispatch(Event $event): void
    {
        static::service()->dispatch($event);
    }

    public static function deferred(Event $event): void
    {
        static::service()->deferred($event);
    }

    public static function dispatchDeferred(): void
    {
        static::service()->dispatchDeferred();
    }

    protected static function service(): EventsService
    {
        return app(EventsService::class);
    }
}
