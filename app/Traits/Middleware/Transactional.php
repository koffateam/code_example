<?php

namespace App\Traits\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Throwable;

trait Transactional
{
    public function handle($object, Closure $next)
    {
        DB::beginTransaction();

        try {
            $result = $next($object);
        } catch (Throwable $e) {
            DB::rollBack(0);
            throw $e;
        }

        DB::commit();

        return $result;
    }
}
