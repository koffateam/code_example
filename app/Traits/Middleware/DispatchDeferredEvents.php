<?php

namespace App\Traits\Middleware;

use App\Helpers\Events;
use Closure;

trait DispatchDeferredEvents
{
    public function handle($object, Closure $next)
    {
        $result = $next($object);

        Events::dispatchDeferred();

        return $result;
    }
}
