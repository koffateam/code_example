<?php

namespace App\Traits;

use ReflectionClass;

trait Named
{
    public function getName(): string
    {
        return (new ReflectionClass($this))->getName();
    }

    public function getShortName(): string
    {
        return (new ReflectionClass($this))->getShortName();
    }
}
