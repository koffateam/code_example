<?php

namespace App\Traits;

trait Configurable
{
    protected array $config = [];

    public function config(string $key = '', $default = null)
    {
        return data_get($this->getConfig(), $key, $default);
    }

    protected function getConfig(): array
    {
        if (!$this->config) {
            $this->config = config($this->getConfigBaseKey(), []);
        }

        return $this->config;
    }

    abstract protected function getConfigBaseKey(): string;
}
