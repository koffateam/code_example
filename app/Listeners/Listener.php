<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;

abstract class Listener implements ShouldQueue
{
    protected const BASE_TAG     = 'listeners';
    protected const CATEGORY_TAG = 'none';

    public function tags(): array
    {
        return [
            static::BASE_TAG.':'.static::class,
            static::BASE_TAG.':category:'.static::CATEGORY_TAG,
        ];
    }
}
