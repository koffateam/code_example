<?php

namespace App\Services\Events;

use App\Events\{Event, EventsCollection};
use App\Services\Service;

class EventsService extends Service
{
    protected EventsCollection $deferredEvents;

    public function __construct()
    {
        $this->deferredEvents = new EventsCollection;
    }

    public function dispatch(Event $event): void
    {
        event($event);
    }

    public function deferred(Event $event): void
    {
        $this->deferredEvents->push($event);
    }

    public function dispatchDeferred(): void
    {
        foreach ($this->deferredEvents as $key => $deferredEvent) {
            $this->dispatch($deferredEvent);
            $this->deferredEvents->forget($key);
        }
    }
}
