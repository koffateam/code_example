<?php

namespace App\Services\HttpSender;

use App\Entities\PSR7\{RequestsCollection, ResponsesCollection};
use App\Services\Service;
use Psr\Http\Message\{RequestInterface, ResponseInterface};

abstract class HttpSender extends Service
{
    /**
     * Send PSR-7 compatible request and get PSR-7 compatible response.
     *
     * @param  RequestInterface  $request  request to send
     *
     * @return ResponseInterface
     */
    abstract public function sendRequest(RequestInterface $request): ResponseInterface;

    /**
     * Send PSR-7 compatible requests and get PSR-7 compatible responses.
     *
     * @param  RequestsCollection  $requests     Collection of requests to send
     * @param  int                 $concurrency  Number of concurrency requests
     *
     * @return ResponsesCollection
     */
    abstract public function sendRequests(RequestsCollection $requests, int $concurrency = 1): ResponsesCollection;
}
