<?php

namespace App\Services\HttpSender;

use App\Traits\Configurable;
use Ackintosh\Ganesha\{Builder as CircuitBreakerBuilder,
    GuzzleMiddleware as CircuitBreakerMiddleware,
    Storage\Adapter\Redis as CircuitBreakerRedisAdapter};
use App\Entities\PSR7\{RequestsCollection, ResponsesCollection};
use Generator;
use GuzzleHttp\{Client as GuzzleClient, Exception\RequestException, HandlerStack, MessageFormatter, Middleware, Pool};
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Kevinrob\GuzzleCache\{CacheMiddleware, Storage\LaravelCacheStorage, Strategy\GreedyCacheStrategy};
use Predis\Client as PredisClient;
use Psr\Http\Message\{RequestInterface, ResponseInterface};
use Psr\Log\LoggerInterface;

class GuzzleHttpSender extends HttpSender
{
    use Configurable;

    private GuzzleClient $guzzleClient;

    public function __construct(GuzzleClient $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
    }

    public function getGuzzleClient(): GuzzleClient
    {
        return $this->guzzleClient;
    }

    public function sendRequest(RequestInterface $request, array $options = []): ResponseInterface
    {
        return $this->guzzleClient->send($request, $options);
    }

    public function sendRequests(RequestsCollection $requests, int $concurrency = 1, array $options = []): ResponsesCollection
    {
        $responses = new ResponsesCollection;

        $pool = new Pool($this->guzzleClient, $this->getRequests($requests, $options), [
            'concurrency' => $concurrency,
            'fulfilled' => function (ResponseInterface $response, int $index) use (&$responses) {
                $responses->put($index, $response);
            },
            'rejected' => function (RequestException $reason, int $index) use (&$responses) {
                if ($response = $reason->getResponse()) {
                    $responses->put($index, $response);
                }
            },
        ]);

        $pool->promise()->wait();

        return $responses;
    }

    public function addCacheMiddleware(CacheRepository $cacheRepository)
    {
        $middleware = new CacheMiddleware(
            new GreedyCacheStrategy(
                new LaravelCacheStorage($cacheRepository),
                $this->config('middleware.cache.ttl')
            )
        );

        $this->getGuzzleHandlerStack()->push($middleware, 'greedy-cache');
    }

    public function addLoggerMiddleware(LoggerInterface $logger)
    {
        $logLevel  = $this->config('middleware.log.level');
        $logFormat = $this->config('middleware.log.format');

        $formatter = new MessageFormatter($logFormat);

        $middleware = Middleware::log($logger, $formatter, $logLevel);

        $this->getGuzzleHandlerStack()->push($middleware, 'logger');
    }

    public function addCircuitBreakerMiddleware(PredisClient $redis)
    {
        $middleware = new CircuitBreakerMiddleware(
            CircuitBreakerBuilder::build([
                'timeWindow'           => $this->config('middleware.circuit_breaker.time_window'),
                'failureRateThreshold' => $this->config('middleware.circuit_breaker.failure_rate_threshold'),
                'minimumRequests'      => $this->config('middleware.circuit_breaker.minimum_requests'),
                'intervalToHalfOpen'   => $this->config('middleware.circuit_breaker.interval_to_half_open'),
                'adapter'              => new CircuitBreakerRedisAdapter($redis),
            ])
        );

        $this->getGuzzleHandlerStack()->push($middleware, 'circuit-breaker');
    }

    protected function getConfigBaseKey(): string
    {
        return 'http_sender.guzzle';
    }

    private function getRequests(RequestsCollection $requests, array $options = []): Generator
    {
        $client = $this->guzzleClient;

        /** @var RequestInterface $request */
        foreach ($requests as $request) {
            yield function() use ($client, $request, $options) {
                return $client->sendAsync($request, $options);
            };
        }
    }

    private function getGuzzleHandlerStack(): HandlerStack
    {
        return $this->getGuzzleClient()->getConfig()['handler'];
    }
}
