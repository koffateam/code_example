<?php

namespace App\Services;

use App\Traits\Named;

abstract class Service
{
    use Named;
}
