<?php

namespace App\Entities\PSR7;

use App\Extend\Illuminate\Support\Collection\TypedCollection;
use Psr\Http\Message\RequestInterface;

class RequestsCollection extends TypedCollection
{
    protected string $itemsType = RequestInterface::class;
}
