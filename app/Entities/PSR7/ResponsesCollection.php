<?php

namespace App\Entities\PSR7;

use App\Extend\Illuminate\Support\Collection\TypedCollection;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

class ResponsesCollection extends TypedCollection
{
    protected string $itemsType = ResponseInterface::class;

    public function getOkResponse($key): ?ResponseInterface
    {
        $response = $this[$key] ?? null;
        if (is_null($response) || $response->getStatusCode() !== Response::HTTP_OK) {
            return null;
        }

        return $response;
    }
}
