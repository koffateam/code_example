<?php

namespace App\Providers\HttpSender;

use App\Providers\ServiceProvider;
use GuzzleHttp\Client as GuzzleClient;
use App\Services\HttpSender\{GuzzleHttpSender, HttpSender};
use Illuminate\Foundation\Application;

class HttpSenderServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(HttpSender::class, function (Application $app) {
            $guzzleHttpSender = new GuzzleHttpSender($app->make(GuzzleClient::class));

            $logger  = $app->make('log')->driver();
            $cache   = $app->make('cache')->store();
            $cbRedis = $app->make('redis')->connection('circuit_breaker')->client();

            $guzzleHttpSender->addLoggerMiddleware($logger);
            $guzzleHttpSender->addCacheMiddleware($cache);
            $guzzleHttpSender->addCircuitBreakerMiddleware($cbRedis);

            return $guzzleHttpSender;
        });

        $this->app->singleton(GuzzleClient::class, function () {
            return new GuzzleClient(config('guzzle', []));
        });
    }

    public function provides(): array
    {
        return [
            HttpSender::class,
            GuzzleClient::class,
        ];
    }
}
