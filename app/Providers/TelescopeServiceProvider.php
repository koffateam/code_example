<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Laravel\Telescope\{IncomingEntry, Telescope, TelescopeApplicationServiceProvider};

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    public function register()
    {
        Telescope::night();

        Telescope::filter(function (IncomingEntry $entry) {
            return true;
        });
    }

    protected function gate()
    {
        Gate::define('viewTelescope', function () {
            return true;
        });
    }

    protected function authorization()
    {
        Telescope::auth(function () {
            return env('TELESCOPE_ENABLED', true);
        });
    }
}
