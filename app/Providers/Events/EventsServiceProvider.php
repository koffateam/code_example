<?php

namespace App\Providers\Events;

use App\Providers\ServiceProvider;
use App\Services\Events\EventsService;

class EventsServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(EventsService::class, function () {
            return new EventsService;
        });
    }

    public function provides(): array
    {
        return [EventsService::class];
    }
}
