<?php

namespace App\Exceptions\Internal\TypedCollection;

use App\Exceptions\Internal\Exception as InternalException;

class Exception extends InternalException
{

}
