<?php

namespace App\Exceptions\Internal;

use App\Exceptions\Exception as AppException;
use Symfony\Component\HttpFoundation\Response;

class Exception extends AppException
{
    protected $code = Response::HTTP_INTERNAL_SERVER_ERROR;
}
