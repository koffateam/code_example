<?php

namespace App\Exceptions\External;

use App\Exceptions\Exception as AppException;
use Symfony\Component\HttpFoundation\Response;

class Exception extends AppException
{
    protected $code = Response::HTTP_BAD_REQUEST;
}
