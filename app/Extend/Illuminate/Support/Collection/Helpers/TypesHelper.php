<?php

namespace App\Extend\Illuminate\Support\Collection\Helpers;

use Illuminate\Support\Collection;
use App\Exceptions\Internal\TypedCollection\IsNotType;

class TypesHelper
{
    const PRIMITIVE_TYPES = [
        'bool', 'int', 'float', 'string', 'numeric', 'scalar',
        'array', 'object', 'callable', 'resource', 'null',
        'double', 'integer', 'long', 'real',
    ];

    public static function isTypePrimitive(string $type): bool
    {
        return in_array(strtolower($type), static::PRIMITIVE_TYPES);
    }

    public static function isType(string $type): bool
    {
        return class_exists($type) || interface_exists($type) || static::isTypePrimitive($type);
    }

    public static function getType($item): string
    {
        $typeAliasesMap = [
            'NULL'    => 'null',
            'double'  => 'float',
            'integer' => 'int',
            'boolean' => 'bool',
        ];

        $itemType = gettype($item);

        if ($itemType === 'object') {
            return get_class($item);
        }

//        if ($itemType === 'resource') {
//            return get_resource_type($item);
//        }

        if (isset($typeAliasesMap[$itemType])) {
            $itemType = $typeAliasesMap[$itemType];
        }

        return $itemType;
    }

    public static function isInstanceOf($item, string $type): bool
    {
        if (!static::isType($type)) {
            throw new IsNotType('Second argument has to be a type.');
        }

        if (static::isTypePrimitive($type)) {
            $checkFunction = "is_{$type}";
            return function_exists($checkFunction) && $checkFunction($item);
        }

        return $item instanceof $type;
    }

    public static function autoDetectType($items): string
    {
        $items = Collection::make($items);

        if ($items->isEmpty()) {
            return '';
        }

        $firstItem = $items->first();
        $type      = static::getType($firstItem);

        foreach ($items as $item) {
            if (!static::isInstanceOf($item, $type)) {
                if ($type = static::tryExtendType($item, $firstItem)) {
                    continue;
                }

                return '';
            }
        }

        return $type;
    }

    protected static function tryExtendType($item, $firstItem): string
    {
        $targetTypes = ['numeric', 'scalar', 'object', 'callable'];

        foreach ($targetTypes as $targetType) {
            if (static::isInstanceOf($firstItem, $targetType) && static::isInstanceOf($item, $targetType)) {
                return $targetType;
            }
        }

        return '';
    }
}
