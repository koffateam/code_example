<?php

namespace App\Extend\Illuminate\Support\Collection\Helpers;

use ReflectionClass;
use ReflectionMethod;
use Illuminate\Support\Collection;
use App\Extend\Illuminate\Support\Collection\TypedCollection;

class MethodsHelper
{
    const RETURN_TYPED_COLLECTION = ['diff', 'diffAssoc', 'diffKeys', 'diffAssocUsing', 'diffKeysUsing', 'diffUsing',
        'take', 'tap', 'unique', 'uniqueStrict', 'values', 'filter', 'forget', 'reverse', 'only', 'shuffle',
        'where', 'whereStrict', 'whereIn', 'whereInStrict', 'whereNotIn', 'whereNotInStrict', 'whereInstanceOf',
        'whereBetween', 'whereNotBetween', 'sort', 'sortBy', 'sortByDesc', 'sortKeys', 'sortKeysDesc', 'slice',
        'splice', 'forPage', 'nth', 'reject', 'keyBy', 'make', 'times', 'push', 'put', 'prepend', 'add', 'wrap',
        'intersect', 'intersectByKeys', 'except', 'each', 'eachSpread', 'dump', 'random'];

    const RETURN_ILLUMINATE_COLLECTION = ['toBase', 'countBy'];

    const RETURN_TYPED_OR_ILLUMINATE_COLLECTION = ['collapse', 'combine', 'concat', 'flatMap', 'flip', 'crossJoin',
        'map', 'mapSpread', 'mapInto', 'mapToDictionary', 'mapToGroups', 'mapWithKeys', 'transform', 'merge', 'union',
        'flatten', 'keys', 'pluck', 'pad', 'zip', 'pipe', 'when', 'whenEmpty', 'whenNotEmpty', 'unless', 'unlessEmpty',
        'unlessNotEmpty', 'duplicates', 'duplicatesStrict'];

    const RETURN_COLLECTION_OF_TYPED_COLLECTIONS = ['groupBy', 'partition', 'split', 'chunk'];

    const RETURN_NOT_COLLECTION = ['some', 'contains', 'containsStrict', 'every', 'has', 'isEmpty', 'isNotEmpty',
        'average', 'avg', 'first', 'get', 'last', 'max', 'median', 'min', 'pop', 'pull', 'search', 'shift',
        'sum', 'implode', 'dd', 'mixin', 'getIterator', 'getCachingIterator', 'all', 'jsonSerialize',
        'toArray', 'mode', 'hasMacro', 'offsetExists', 'count', 'offsetGet', 'toJson', 'macro', 'offsetSet',
        'offsetUnset', 'unwrap', 'proxy', 'firstWhere', 'reduce', 'join'];

    public static function getIlluminateCollectionMethods(): TypedCollection
    {
        return static::getPublicMethods(Collection::class);
    }

    public static function getTypedCollectionMethods(): TypedCollection
    {
        return static::getPublicMethods(TypedCollection::class);
    }

    protected static function getPublicMethods(string $class): TypedCollection
    {
        $methods = (new ReflectionClass($class))->getMethods(ReflectionMethod::IS_PUBLIC);
        $methods = TypedCollection::make($methods, ReflectionMethod::class);

        return $methods;
    }

    public static function isIlluminateCollectionMethod(string $method): bool
    {
        return static::getIlluminateCollectionMethods()->pluck('name')->contains($method) ||
            Collection::hasMacro($method);
    }

    public static function isTypedCollectionMethod(string $method): bool
    {
        return static::getTypedCollectionMethods()->pluck('name')->contains($method) ||
            TypedCollection::hasMacro($method);
    }

    public static function isMethodMagic(string $method): bool
    {
        return mb_substr($method, 0, 2) === '__';
    }

    public static function isMethodReturnTypedCollection(string $method): bool
    {
        return in_array($method, static::RETURN_TYPED_COLLECTION);
    }

    public static function isMethodReturnIlluminateCollection(string $method): bool
    {
        return in_array($method, static::RETURN_ILLUMINATE_COLLECTION);
    }

    public static function isMethodReturnTypedOrIlluminateCollection(string $method): bool
    {
        return in_array($method, static::RETURN_TYPED_OR_ILLUMINATE_COLLECTION);
    }

    public static function isMethodReturnCollectionOfTypedCollections(string $method): bool
    {
        return in_array($method, static::RETURN_COLLECTION_OF_TYPED_COLLECTIONS);
    }

    public static function isMethodReturnNotCollection(string $method): bool
    {
        return in_array($method, static::RETURN_NOT_COLLECTION);
    }

    public static function isMethodIdentified(string $method): bool
    {
        return
            static::isMethodReturnTypedCollection($method) ||
            static::isMethodReturnIlluminateCollection($method) ||
            static::isMethodReturnTypedOrIlluminateCollection($method) ||
            static::isMethodReturnCollectionOfTypedCollections($method) ||
            static::isMethodReturnNotCollection($method);
    }
}
