<?php

namespace App\Extend\Illuminate\Support\Collection;

use Countable;
use ArrayAccess;
use ArrayIterator;
use CachingIterator;
use JsonSerializable;
use IteratorAggregate;
use InvalidArgumentException;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\{Arrayable, Jsonable};
use App\Extend\Illuminate\Support\Collection\Helpers\{TypesHelper, MethodsHelper};
use App\Exceptions\Internal\TypedCollection\{WrongGroup, WrongItemsType, WrongItemType};

/**
 * @method Collection countBy(callable|null $callback = null)
 * @method Collection toBase()
 * @method array all()
 * @method array|null mode(string|array|null $key = null)
 * @method bool contains(mixed $key, mixed $operator = null, mixed $value = null)
 * @method bool containsStrict(mixed $key, mixed $value = null)
 * @method bool every(string|callable $key, mixed $operator = null, mixed $value = null)
 * @method bool has(mixed $key)
 * @method bool isEmpty()
 * @method bool isNotEmpty()
 * @method bool some(mixed $key, mixed $operator = null, mixed $value = null)
 * @method mixed average(callable|string|null $callback = null)
 * @method mixed avg(callable|string|null $callback = null)
 * @method mixed first(callable|null $callback = null, mixed $default = null)
 * @method mixed firstWhere(string $key, mixed $operator = null, mixed $value = null)
 * @method mixed get(mixed $key, mixed $default = null)
 * @method mixed last(callable|null $callback = null, mixed $default = null)
 * @method mixed max(callable|string|null $callback = null)
 * @method mixed median(string|array|null $key = null)
 * @method mixed min(callable|string|null $callback = null)
 * @method mixed pop()
 * @method mixed pull(mixed $key, mixed $default = null)
 * @method mixed reduce(callable $callback, mixed $initial = null)
 * @method mixed search(mixed $value, bool $strict = false)
 * @method mixed shift()
 * @method mixed sum(callable|string|null $callback = null)
 * @method static chunk(int $size)
 * @method static diff(mixed $items)
 * @method static diffAssoc(mixed $items)
 * @method static diffAssocUsing(mixed $items, callable $callback)
 * @method static diffKeys(mixed $items)
 * @method static diffKeysUsing(mixed $items, callable $callback)
 * @method static diffUsing(mixed $items, callable $callback)
 * @method static dump()
 * @method static each(callable $callback)
 * @method static eachSpread(callable $callback)
 * @method static except(\Illuminate\Support\Collection|mixed $keys)
 * @method static filter(callable|null $callback = null)
 * @method static forPage(int $page, int $perPage)
 * @method static forget(string|array $keys)
 * @method static groupBy(array|callable|string $groupBy, bool $preserveKeys = false)
 * @method static intersect(mixed $items)
 * @method static intersectByKeys(mixed $items)
 * @method static keyBy(callable|string $keyBy)
 * @method static nth(int $step, int $offset = 0)
 * @method static only(mixed $keys)
 * @method static partition(callable|string $key, mixed $operator = null, mixed $value = null)
 * @method static reject(callable|mixed $callback = true)
 * @method static reverse()
 * @method static shuffle(int $seed = null)
 * @method static slice(int $offset, int $length = null)
 * @method static sort(callable|null $callback = null)
 * @method static sortBy(callable|string $callback, int $options = 0, bool $descending = false)
 * @method static sortByDesc(callable|string $callback, int $options = 0)
 * @method static sortKeys(int $options = 0, bool $descending = false)
 * @method static sortKeysDesc(int $options = 0)
 * @method static splice(int $offset, int|null $length = null, mixed $replacement = [])
 * @method static split(int $numberOfGroups)
 * @method static take(int $limit)
 * @method static tap(callable $callback)
 * @method static unique(string|callable|null $key = null, bool $strict = false)
 * @method static uniqueStrict(string|callable|null $key = null)
 * @method static values()
 * @method static where(string $key, mixed $operator = null, mixed $value = null)
 * @method static whereBetween(string $key, array $values)
 * @method static whereIn(string $key, mixed $values, bool $strict = false)
 * @method static whereInStrict(string $key, mixed $values)
 * @method static whereInstanceOf(string $type)
 * @method static whereNotBetween(string $key, array $values)
 * @method static whereNotIn(string $key, mixed $values, bool $strict = false)
 * @method static whereNotInStrict(string $key, mixed $values)
 * @method static whereStrict(string $key, mixed $value)
 * @method static|Collection collapse()
 * @method static|Collection combine(mixed $values)
 * @method static|Collection concat(iterable $source)
 * @method static|Collection crossJoin(mixed ...$lists)
 * @method static|Collection duplicates(callable|null $callback = null, bool $strict = false)
 * @method static|Collection duplicatesStrict(callable|null $callback = null)
 * @method static|Collection flatMap(callable $callback)
 * @method static|Collection flatten(int $depth = INF)
 * @method static|Collection flip()
 * @method static|Collection keys()
 * @method static|Collection map(callable $callback)
 * @method static|Collection mapInto(string $class)
 * @method static|Collection mapSpread(callable $callback)
 * @method static|Collection mapToDictionary(callable $callback)
 * @method static|Collection mapToGroups(callable $callback)
 * @method static|Collection mapWithKeys(callable $callback)
 * @method static|Collection merge(mixed $items)
 * @method static|Collection pad(int $size, mixed $value)
 * @method static|Collection pluck(string|array $value, string|null $key = null)
 * @method static|Collection transform(callable $callback)
 * @method static|Collection union(mixed $items)
 * @method static|Collection zip(mixed ...$items)
 * @method static|mixed unless(bool $value, callable $callback, callable $default = null)
 * @method static|mixed unlessEmpty(callable $callback, callable $default = null)
 * @method static|mixed unlessNotEmpty(callable $callback, callable $default = null)
 * @method static|mixed whenEmpty(callable $callback, callable $default = null)
 * @method static|mixed whenNotEmpty(callable $callback, callable $default = null)
 * @method string implode(string $value, string $glue = null)
 * @method string join(string $glue, string $finalGlue = '')
 * @method void dd(mixed ...$args)
 * @method void mixin(object $mixin, bool $replace = true)
 */
class TypedCollection implements ArrayAccess, Arrayable, Countable, IteratorAggregate, Jsonable, JsonSerializable
{
    protected string     $itemsType;
    protected Collection $illuminateCollection;

    public function __construct($items = [], string $itemsType = null)
    {
        $this->illuminateCollection = Collection::make($items);

        if (!is_null($this->itemsType) && !is_string($this->itemsType)) {
            throw new WrongItemsType('Items type has to be a string.');
        }

        $this->setItemsType($itemsType ?? $this->itemsType ?? '');
    }

    public function __call(string $method, array $parameters)
    {
        if (MethodsHelper::isMethodReturnTypedCollection($method)) {
            return $this->proxyTypedCollectionMethods($method, $parameters);
        } elseif (MethodsHelper::isMethodReturnCollectionOfTypedCollections($method)) {
            return $this->proxyGroupMethods($method, $parameters);
        } elseif (MethodsHelper::isMethodReturnTypedOrIlluminateCollection($method)) {
            return $this->proxyDoubleBehaviorMethods($method, $parameters);
        } elseif (MethodsHelper::isMethodReturnIlluminateCollection($method)) {
            return $this->proxyIlluminateCollectionMethods($method, $parameters);
        } else {
            return $this->proxyIlluminateCollectionMethods($method, $parameters);
        }
    }

    public function __get($parameter)
    {
        return $this->illuminateCollection->$parameter;
    }

    public function __toString(): string
    {
        return $this->toJson();
    }

    protected function proxyTypedCollectionMethods(string $method, array $parameters): TypedCollection
    {
        return new static(
            $this->proxyIlluminateCollectionMethods($method, $parameters),
            $this->itemsType
        );
    }

    protected function proxyGroupMethods(string $method, array $parameters): TypedCollection
    {
        /** @var Collection $groupsCollection */
        $groupsCollection = $this->proxyIlluminateCollectionMethods($method, $parameters);

        $typedGroupsCollection = new static([], TypedCollection::class);
        foreach ($groupsCollection as $key => $group) {
            if (!($group instanceof Collection)) {
                throw new WrongGroup('Every group has to be a collection.');
            }

            $typedGroupsCollection->put($key, new static($group, $this->itemsType));
        }

        return $typedGroupsCollection;
    }

    protected function proxyIlluminateCollectionMethods(string $method, array $parameters)
    {
        return $this->illuminateCollection->{$method}(...$parameters);
    }

    protected function proxyDoubleBehaviorMethods(string $method, array $parameters)
    {
        $result = $this->proxyIlluminateCollectionMethods($method, $parameters);

        return $this->tryConvertToTypedCollection($result);
    }

    protected function tryConvertToTypedCollection($result)
    {
        try {
            return new static($result, $this->itemsType);
        } catch (WrongItemType $e) {
            return $result;
        }
    }

    public function getItemsType(): string
    {
        return $this->itemsType;
    }

    protected function setItemsType(string $itemsType): TypedCollection
    {
        if ($itemsType === '') {
            $itemsType = TypesHelper::autoDetectType($this->illuminateCollection);
        }

        $this->checkItemsType($itemsType);
        $this->itemsType = $itemsType;

        $this->checkItems($this->illuminateCollection);

        return $this;
    }

    protected function checkItemsType(string $itemsType)
    {
        if ($itemsType === '') {
            throw new WrongItemsType('Items type is not set. Autodetect failed.');
        }

        if (!TypesHelper::isType($itemsType)) {
            throw new WrongItemsType(
                'Items type has to be a existed class name, a existed interface or one of primitive types. '.
                "Received: {$itemsType}."
            );
        }
    }

    protected function checkItem($item)
    {
        if (!TypesHelper::isInstanceOf($item, $this->itemsType)) {
            $itemType = TypesHelper::getType($item);
            throw new WrongItemType("Wrong item type, expected: {$this->itemsType}, received: {$itemType}.");
        }
    }

    protected function checkItems($items)
    {
        foreach (Collection::make($items) as $item) {
            $this->checkItem($item);
        }
    }

    public static function make($items = [], string $itemsType = null): TypedCollection
    {
        return new static($items, $itemsType);
    }

    public static function times(int $amount, callable $callback = null, string $itemsType = null): TypedCollection
    {
        return new static(Collection::times($amount, $callback), $itemsType);
    }

    public static function wrap($value, string $itemsType = null): TypedCollection
    {
        return $value instanceof Collection || $value instanceof TypedCollection
            ? new static($value, $itemsType)
            : new static(Collection::wrap($value), $itemsType);
    }

    public static function unwrap($value)
    {
        return $value instanceof Collection || $value instanceof TypedCollection
            ? $value->all()
            : $value;
    }

    public static function macro(string $name, callable $macro)
    {
        Collection::macro($name, $macro);
    }

    public static function hasMacro(string $name): bool
    {
        return Collection::hasMacro($name);
    }

    public static function proxy(string $method)
    {
        Collection::proxy($method);
    }

    public function toArray(): array
    {
        return $this->illuminateCollection->toArray();
    }

    public function getIterator(): ArrayIterator
    {
        return $this->illuminateCollection->getIterator();
    }

    public function getCachingIterator($flags = CachingIterator::CALL_TOSTRING): CachingIterator
    {
        return new CachingIterator($this->getIterator(), $flags);
    }

    public function offsetExists($key): bool
    {
        return $this->illuminateCollection->offsetExists($key);
    }

    public function offsetGet($key)
    {
        return $this->illuminateCollection->offsetGet($key);
    }

    public function offsetSet($key, $value)
    {
        $this->checkItem($value);
        $this->illuminateCollection->offsetSet($key, $value);
    }

    public function offsetUnset($key)
    {
        $this->illuminateCollection->offsetUnset($key);
    }

    public function count(): int
    {
        return $this->illuminateCollection->count();
    }

    public function toJson($options = 0): string
    {
        return $this->illuminateCollection->toJson($options);
    }

    public function jsonSerialize(): array
    {
        return $this->illuminateCollection->jsonSerialize();
    }

    public function prepend($value, $key = null): TypedCollection
    {
        $this->checkItem($value);
        $this->illuminateCollection->prepend($value, $key);

        return $this;
    }

    public function push($value): TypedCollection
    {
        $this->checkItem($value);
        $this->illuminateCollection->push($value);

        return $this;
    }

    public function put($key, $value): TypedCollection
    {
        $this->checkItem($value);
        $this->illuminateCollection->put($key, $value);

        return $this;
    }

    public function add($item): TypedCollection
    {
        $this->checkItem($item);
        $this->illuminateCollection->add($item);

        return $this;
    }

    /**
     * Get one or a specified number of items randomly from the collection.
     *
     * @param  int|null  $number
     * @return mixed
     *
     * @throws InvalidArgumentException
     */
    public function random(int $number = 1)
    {
        if ($number === 0) {
            return new static([], $this->itemsType);
        }

        if ($number > ($count = $this->count()) || $number < 1) {
            throw new InvalidArgumentException(
                "You requested {$number} random items, but the number of items has to be ".
                "between 1 and the number of items in the collection. There are {$count} items in the collection."
            );
        }

        $keys = array_rand($this->illuminateCollection->all(), $number);

        if (count(func_get_args()) === 0) {
            return $this->illuminateCollection->all()[$keys];
        }

        return new static(
            array_intersect_key($this->illuminateCollection->all(), array_flip(array_wrap($keys))),
            $this->itemsType
        );
    }

    /**
     * Apply the callback if the value is truthy.
     *
     * @param  bool  $value
     * @param  callable  $callback
     * @param  callable  $default
     * @return mixed
     */
    public function when($value, callable $callback, callable $default = null)
    {
        if ($value) {
            $result = $callback($this, $value);
        } elseif ($default) {
            $result = $default($this, $value);
        }

        if (!isset($result)) {
            return $this;
        }

        if ($result instanceof static || $result instanceof Collection) {
            return $this->tryConvertToTypedCollection($result);
        }

        return $result;
    }

    /**
     * Pass the collection to the given callback and return the result.
     *
     * @param  callable $callback
     * @return mixed
     */
    public function pipe(callable $callback)
    {
        $result = $callback($this);

        if ($result instanceof static || $result instanceof Collection) {
            return $this->tryConvertToTypedCollection($result);
        }

        return $result;
    }
}
