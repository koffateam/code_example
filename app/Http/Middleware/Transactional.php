<?php

namespace App\Http\Middleware;

use App\Traits\Middleware\Transactional as TransactionalTrait;

class Transactional
{
    use TransactionalTrait;
}
