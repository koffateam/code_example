<?php

namespace App\Http\Middleware;

use App\Traits\Middleware\DispatchDeferredEvents as DispatchDeferredEventsTrait;

class DispatchDeferredEvents
{
    use DispatchDeferredEventsTrait;
}
