<?php

namespace App\Events;

abstract class Event
{
    protected const BASE_TAG     = 'events';
    protected const CATEGORY_TAG = 'none';

    public function tags(): array
    {
        return [
            static::BASE_TAG.':'.static::class,
            static::BASE_TAG.':category:'.static::CATEGORY_TAG,
        ];
    }
}
