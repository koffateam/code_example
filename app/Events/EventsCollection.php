<?php

namespace App\Events;

use App\Extend\Illuminate\Support\Collection\TypedCollection;

class EventsCollection extends TypedCollection
{
    protected string $itemsType = Event::class;
}
