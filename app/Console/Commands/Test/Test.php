<?php

namespace App\Console\Commands\Test;

use App\Console\Commands\Command;
use App\Constants\RequestMethods;
use App\Entities\PSR7\RequestsCollection;
use GuzzleHttp\Psr7\Request;
use App\Services\HttpSender\HttpSender;
use Symfony\Component\HttpFoundation\Response;

class Test extends Command
{
    protected $signature   = 'test:test';
    protected $description = 'Test command';

    public function handle(HttpSender $httpSender)
    {
        $this->testRequest($httpSender);
        $this->testAsyncRequests($httpSender);
    }

    private function testRequest(HttpSender $httpSender)
    {
        $url = 'ya.ru';

        $response = $httpSender->sendRequest(
            new Request(RequestMethods::GET, $url)
        );

        if ($response->getStatusCode() === Response::HTTP_OK) {
            $this->info("Success request for url {$url}");
        } else {
            $this->error("Request error for url {$url}");
        }
    }

    private function testAsyncRequests(HttpSender $httpSender)
    {
        $urls = ['google.ru', 'qweqwe123qweqwe.com'];

        $requests = new RequestsCollection;
        foreach ($urls as $url) {
            $requests->push(new Request(RequestMethods::GET, $url));
        }

        $responses = $httpSender->sendRequests($requests);

        foreach ($urls as $key => $url) {
            $response = $responses->getOkResponse($key);

            if (is_null($response) || $response->getStatusCode() !== Response::HTTP_OK) {
                $this->error("Request error for url {$url}");
            } else {
                $this->info("Success request for url {$url}");
            }
        }
    }
}
