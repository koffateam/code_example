<?php

namespace App\Console\Commands;

use App\Console\Middleware\{DispatchDeferredEvents, Transactional};
use App\Traits\Named;
use Illuminate\Console\Command as IlluminateCommand;
use Illuminate\Pipeline\Pipeline;
use Symfony\Component\Console\{Input\InputInterface, Output\OutputInterface};

abstract class Command extends IlluminateCommand
{
    use Named;

    protected array $middleware = [
        DispatchDeferredEvents::class,
        Transactional::class,
    ];

    protected function middleware(): array
    {
        return $this->middleware;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        return app(Pipeline::class)
            ->send($this)
            ->through($this->middleware())
            ->then(function (Command $command) {
                return (int) $command->laravel->call([$this, 'handle']);
            });
    }
}
