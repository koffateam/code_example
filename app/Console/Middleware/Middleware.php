<?php

namespace App\Console\Middleware;

use App\Console\Commands\Command;
use Closure;

abstract class Middleware
{
    abstract public function handle(Command $command, Closure $next);
}
