<?php

namespace App\Console\Middleware;

use App\Traits\Middleware\DispatchDeferredEvents as DispatchDeferredEventsTrait;

class DispatchDeferredEvents extends Middleware
{
    use DispatchDeferredEventsTrait;
}
