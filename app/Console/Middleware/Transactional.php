<?php

namespace App\Console\Middleware;

use App\Traits\Middleware\Transactional as TransactionalTrait;

class Transactional extends Middleware
{
    use TransactionalTrait;
}
