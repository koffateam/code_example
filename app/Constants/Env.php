<?php

namespace App\Constants;

class Env extends ConstantsLib
{
    public const DEVELOPMENT = 'development';
    public const PRODUCTION  = 'production';

    public const LIST = [
        self::DEVELOPMENT,
        self::PRODUCTION,
    ];
}
