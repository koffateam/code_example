<?php

namespace App\Constants;

class Queues extends ConstantsLib
{
    public const TRIES       = 1;
    public const MAX_TIMEOUT = 60 * 10;
    public const RETRY_AFTER = self::MAX_TIMEOUT + 10;

    public const DEFAULT = 'default';

    public const LIST = [
        self::DEFAULT,
    ];
}
