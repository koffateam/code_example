APP_PREFIX          := code_example
VARIABLES_FILE_NAME := .env
PROJECT_DIR         := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
VARIABLES_FILE_PATH := $(PROJECT_DIR)/$(VARIABLES_FILE_NAME)
ENV                 := $(shell grep APP_ENV $(VARIABLES_FILE_NAME) | cut -d '=' -f2)
DOCKER_COMPOSE_FILE := docker-compose-$(ENV).yml

ARGS     = $(if $(filter-out $@,$(MAKECMDGOALS)),$(filter-out $@,$(MAKECMDGOALS)),)
ARGS_ENV = $(foreach ARG,$(ARGS),$(APP_PREFIX)_$(ARG)_$(ENV))
ARG1     := $(if $(word 2, $(ARGS)),$(word 2, $(ARGS)),)
ARG2     := $(if $(word 3, $(ARGS)),$(word 3, $(ARGS)),)

.SILENT: ;               # do not print commands
.ONESHELL: ;             # all lines of the recipe will be given to a single invocation of the shell
.NOTPARALLEL: ;          # run serially
.EXPORT_ALL_VARIABLES: ; # export all variables to child processes by default

help: ## Show help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

check: ## Check existing docker-compose-{env}.yml and .env files
ifeq ($(wildcard $(VARIABLES_FILE_PATH)),)
	$(error Configuration file $(VARIABLES_FILE_NAME) not found.)
endif
	docker-compose -f $(DOCKER_COMPOSE_FILE) config -q

build: ## Build images from docker-compose-{env}.yml or specified image
	make check
	docker-compose -f $(DOCKER_COMPOSE_FILE) build $(ARGS_ENV)

start: ## Start images from docker-compose-{env}.yml or specified image
	make check
	docker-compose -f $(DOCKER_COMPOSE_FILE) start $(ARGS_ENV)

stop: ## Start images from docker-compose-{env}.yml or specified image
	make check
	docker-compose -f $(DOCKER_COMPOSE_FILE) stop $(ARGS_ENV)

rm: ## Remove images from docker-compose-{env}.yml or specified image
	make check
	docker-compose -f $(DOCKER_COMPOSE_FILE) rm -f $(ARGS_ENV)

status: ## Images status from docker-compose-{env}.yml
	make check
	docker-compose -f $(DOCKER_COMPOSE_FILE) ps

up: ## Build and start images from docker-compose-{env}.yml or specified image
	make check
	docker-compose -f $(DOCKER_COMPOSE_FILE) up -d $(ARGS_ENV)

down: ## Stop and remove images from docker-compose-{env}.yml or specified image
	make check
	make stop $(ARGS)
	make rm $(ARGS)

restart: ## Stop and start again images from docker-compose-{env}.yml or specified image
	make check
	make stop $(ARGS)
	make start $(ARGS)

reup: ## Build, stop, remove, start again images from docker-compose-{env}.yml or specified image
	make check
	make build $(ARGS)
	make down $(ARGS)
	make up $(ARGS)

log: ## Show logs for images from docker-compose-{env}.yml or specified image
	make check
	docker-compose -f $(DOCKER_COMPOSE_FILE) logs --tail=$(if $(t),$(t),0) --follow $(ARGS_ENV)

shell: ## Open bash for specified image
	docker-compose -f $(DOCKER_COMPOSE_FILE) exec $(APP_PREFIX)_${ARG1}_$(ENV) $(if $(c),$(c),bash)

composer: ## Run composer command
	docker-compose -f $(DOCKER_COMPOSE_FILE) exec $(APP_PREFIX)_backend_$(ENV) composer $(ARGS)

migrate: ## Run migrations
	docker-compose -f $(DOCKER_COMPOSE_FILE) exec $(APP_PREFIX)_backend_$(ENV) php artisan migrate --force --no-interaction

rollback: ## Rollback migrations
	docker-compose -f $(DOCKER_COMPOSE_FILE) exec $(APP_PREFIX)_backend_$(ENV) php artisan migrate:rollback --force --no-interaction

command: ## Run artisan command
	docker-compose -f $(DOCKER_COMPOSE_FILE) exec $(APP_PREFIX)_backend_$(ENV) php artisan $(ARGS)

tinker: ## Run tinker
	docker-compose -f $(DOCKER_COMPOSE_FILE) exec $(APP_PREFIX)_backend_$(ENV) php artisan tinker

reload_horizon: ## Restart Laravel Horizon
	docker-compose -f $(DOCKER_COMPOSE_FILE) exec $(APP_PREFIX)_horizon_$(ENV) php artisan horizon:terminate

%:
	@:
