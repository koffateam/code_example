<?php

return [
    'connect_timeout' => 3.0,
    'timeout'         => 10.0,

    'headers' => [
        'User-Agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 ' .
            '(KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13'
    ],

    'allow_redirects' => [
        'track_redirects' => true,
    ],
];
