<?php

use Psr\Log\LogLevel;

return [

    'guzzle' => [
        'middleware' => [
            'log' => [
                'level'  => LogLevel::DEBUG,
                'format' => 'HttpSender: {method} {uri} code: {code}, error: {error}',
            ],

            'cache' => [
                'ttl' => 30 * 60,
            ],

            'circuit_breaker' => [
                'time_window'            => 30, // the interval in time (seconds) that evaluate the thresholds
                'failure_rate_threshold' => 50, // the failure rate threshold in percentage that changes CircuitBreaker's state to OPEN
                'minimum_requests'       => 10, // the minimum number of requests to detect failures
                'interval_to_half_open'  => 5,  // the interval (seconds) to change CircuitBreaker's state from `OPEN` to `HALF_OPEN`
            ],
        ],
    ],

];
